import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitleHeaderComponent } from './headers/title-header/title-header.component';
import { NavHeaderComponent } from './headers/nav-header/nav-header.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './headers/footer/footer.component';
import { LoginComponent } from './modules/login/component/login/login.component';
import { GuestHeaderComponent } from './headers/guest-header/guest-header.component';
import { TokenInterceptorInterceptor } from './auth/interceptor/token-interceptor.interceptor';
import { SignupComponent } from './modules/signup/components/signup/signup.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { AuthGuardService } from './auth/services/auth-guard.service';
import { commonApiService } from './services/common-api.service';

import { OrderDetailsModule } from './modules/order-details/order-details.module';
import { OrdersReviewModule } from './modules/orders-review/orders-review.module';
import { OrdersToFulfillModule } from './modules/orders-to-fulfill/orders-to-fulfill.module';
import { SummaryModule } from './modules/summary/summary.module';
import { FindOldestInventoryModule } from './modules/find-oldest-inventory/find-oldest-inventory.module';
import { RedHeaderComponent } from './headers/red-header/red-header.component';
@NgModule({
  declarations: [
    AppComponent,
    TitleHeaderComponent,
    NavHeaderComponent,
    FooterComponent,
    LoginComponent,
    GuestHeaderComponent,
    SignupComponent,
    RedHeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OrderDetailsModule,
    OrdersReviewModule,
    OrdersToFulfillModule,
    SummaryModule,
    FindOldestInventoryModule,

    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatListModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatListModule,
    MatTabsModule,
    RouterModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorInterceptor,
      multi: true,
    }, AuthGuardService, commonApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
