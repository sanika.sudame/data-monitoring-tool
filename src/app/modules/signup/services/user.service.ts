import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userDataSource = new BehaviorSubject({email : '', password : ''});
  currentUserData = this.userDataSource.asObservable();
  constructor(private http: HttpClient, private commonApiServiceObj: commonApiService) { }

  registerUser(name: any, email: any, phone: any, password: any, c_password: any){
    const data = { name: name, email: email, phone: phone, password: password, c_password: c_password }
    const url = `${this.commonApiServiceObj.getBaseUrl()}/register`;
    return this.http.post(url, data)
  }
}
