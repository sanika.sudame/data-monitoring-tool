import { Component, Input } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { UserService } from '../../services/user.service'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  // userData: any;
  userData = {
    name: '',
    phone: '',
    email: '',
    password: '',
    c_password: ''
  }
  constructor(private user: UserService) { }

  ngOnInit() {
    // this.user.currentUserData.subscribe(userData => userData)
  }

  signUp() {
    console.log(this.userData);
    this.user.registerUser(this.userData.name, this.userData.email, this.userData.phone, this.userData.password, this.userData.c_password)
      .subscribe((data: any) => {
        document.getElementById("successMessage")?.classList.remove("hidden")
        console.log(data);
      }, (error: any) => {
        console.log(error)
      })
    this.userData.name='';
    this.userData.email='';
    this.userData.phone='';
    this.userData.password='';
    this.userData.c_password='';
  }

}