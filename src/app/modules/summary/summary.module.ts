import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryComponent } from './components/summary/summary.component';

@NgModule({
  declarations: [
    SummaryComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SummaryModule { }
