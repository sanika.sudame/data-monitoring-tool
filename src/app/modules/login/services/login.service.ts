import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private commonApiServiceObj: commonApiService) { }

  myLogin(email: any, password: any){
    const data = { 'email': email, 'password': password};
    const url = `${this.commonApiServiceObj.getBaseUrl()}/login`;
    return this.http.post(url, data);
  }
}
