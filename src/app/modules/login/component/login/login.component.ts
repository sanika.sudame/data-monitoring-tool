import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import { AuthService } from '../../../../auth/services/auth.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(public route: Router,
    public loginServiceObj: LoginService,
    public authServiceObj: AuthService,
    private isLoggedInService: SharedService
    ) { }

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })

  // red label for scan form fields
  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }

  login() {
    this.loginServiceObj.myLogin(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe((data: any) => {
        sessionStorage.setItem('userId', data.userId);
        sessionStorage.setItem('userName', data.userName.toString());
        this.authServiceObj.setAuthToken(data.token);
        this.isLoggedInService.login();
        this.route.navigate(['/orders-to-fulfill']);
      }, (error: any) => {
        console.log(error)
        alert("Login failed")
      })
  }

}
