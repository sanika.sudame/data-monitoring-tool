import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersToFulfillComponent } from './orders-to-fulfill.component';

describe('OrdersToFulfillComponent', () => {
  let component: OrdersToFulfillComponent;
  let fixture: ComponentFixture<OrdersToFulfillComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrdersToFulfillComponent]
    });
    fixture = TestBed.createComponent(OrdersToFulfillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
