import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersToFulfillService } from '../../services/orders-to-fulfill.service';

@Component({
  selector: 'app-orders-to-fulfill',
  templateUrl: './orders-to-fulfill.component.html',
  styleUrls: ['./orders-to-fulfill.component.scss']
})
export class OrdersToFulfillComponent{
  tableData: any;

  constructor(public orderToFulfillServiceObject: OrdersToFulfillService, public router: Router) {
  }

  ngOnInit() {
    this.orderToFulfillServiceObject.getData().subscribe((data: any) => {
      this.tableData = data.payload;
    },(error)=>{
      if(error.status == 401){
        this.router.navigate(['/login']);
      }
      else{
        alert(Object.entries(error));
      }
    })
  }


  onOrderIdClick(orderId: any) {
    // Encode the orderId before navigating
    this.router.navigate(['/order-details', orderId]);
  }
}
