import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersToFulfillComponent } from './components/orders-to-fulfill/orders-to-fulfill.component';
import { LegendComponent } from './components/legend/legend.component';

@NgModule({
  declarations: [
    OrdersToFulfillComponent,
    LegendComponent
  ],
  imports: [
    CommonModule
  ]
})
export class OrdersToFulfillModule { }
