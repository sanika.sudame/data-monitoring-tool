import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersToFulfillService {

  constructor(private http: HttpClient, private commonApiServiceObj: commonApiService) { }

  getData(){
    const url = `${this.commonApiServiceObj.getBaseUrl()}/ordersToFulfill`;
    return this.http.get(url);
  }
}
