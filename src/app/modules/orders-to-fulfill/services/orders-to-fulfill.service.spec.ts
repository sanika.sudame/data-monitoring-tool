import { TestBed } from '@angular/core/testing';

import { OrdersToFulfillService } from './orders-to-fulfill.service';

describe('OrdersToFulfillService', () => {
  let service: OrdersToFulfillService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrdersToFulfillService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
