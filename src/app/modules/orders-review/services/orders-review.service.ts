import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersReviewService {
  
  constructor( private http: HttpClient, private commonApiServiceObj: commonApiService) { }

  getReviewData(){
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getAllReviews`;
    return this.http.get(url);
  }

  mapUpc(upcCode: any, mappedUpcCode: any){
    const data = { upcCode: upcCode, mappedUpcCode: mappedUpcCode};
    const url = `${this.commonApiServiceObj.getBaseUrl()}/mapUpc`;
    return this.http.post(url, data);
  }
}