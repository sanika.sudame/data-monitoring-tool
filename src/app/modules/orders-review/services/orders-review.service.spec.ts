import { TestBed } from '@angular/core/testing';

import { OrdersReviewService } from './orders-review.service';

describe('OrdersReviewService', () => {
  let service: OrdersReviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrdersReviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
