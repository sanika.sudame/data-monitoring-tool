import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersReviewComponent } from './components/orders-review/orders-review.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';



@NgModule({
  declarations: [
    OrdersReviewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule
  ],
  exports: [
  ]
})
export class OrdersReviewModule { }
