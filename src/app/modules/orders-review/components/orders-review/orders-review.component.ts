import { Component } from '@angular/core';
import { OrderDetailsService } from 'src/app/modules/order-details/services/order-details.service';
import { OrdersReviewService } from '../../services/orders-review.service';

@Component({
  selector: 'app-orders-review',
  templateUrl: './orders-review.component.html',
  styleUrls: ['./orders-review.component.scss']
})
export class OrdersReviewComponent {

  reviewData: any;
  allUpcValues: any;
  upcDescription: any;
  mappedValue: any;
  selectedUpc = 'option2';
  selected: any;
  mappedUpc: any;

  constructor(private ordersReviewServiceObj: OrdersReviewService) { }

  ngOnInit() {
    this.getReviewData();
  }

  getReviewData(){
    this.ordersReviewServiceObj.getReviewData().subscribe((data: any) => {
      this.reviewData = data.payload;
      this.allUpcValues = data.allUpcValues;
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  updateLabel(event: any): void {
    this.selected = event.target.value;
    const [description, upcCode] = this.selected.split('||');
    this.mappedValue = parseInt(upcCode);
    const upcLabelElement = document.getElementById("upcLabel");
    if (upcLabelElement) {
      upcLabelElement.innerHTML = description;
    }
  }

  mapUpc() {
    console.log(this.selectedUpc)
    console.log(this.mappedValue);
    this.ordersReviewServiceObj.mapUpc(this.selectedUpc, this.mappedValue)
      .subscribe((data)=>{
        console.log(data);
        this.getReviewData();
      }, (error)=>{
        console.log(error);
      })
  }
}
