import { TestBed } from '@angular/core/testing';

import { tableDataService } from './table-data.service';

describe('OrderDetailsTableService', () => {
  let service: tableDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(tableDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
