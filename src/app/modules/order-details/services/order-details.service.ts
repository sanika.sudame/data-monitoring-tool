import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class OrderDetailsService {

  constructor(public http: HttpClient, private commonApiServiceObj: commonApiService) { }

  // get all upc values to order id
  getUpcValues(orderId: any) {
    const data = { 'orderId': orderId };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getUpcValues`;
    return this.http.post(url, data);
  }

  // api call to scan for both pick and pack status
  scanSerialNumber(orderId: any, upc: any, serialNumber: any, userId: any, packageUid: any) {
    let setPackage;
    // set pack if package is selected or else set pick
    if(packageUid){
      setPackage = 1;
    }
    else{
      setPackage = 0;
    }
    const data = {
      'orderId': orderId,
      'upc': upc,
      'serialNumber': serialNumber,
      'userId': userId,
      'packageUid': packageUid,
      'setPackage': setPackage
    }
    const url = `${this.commonApiServiceObj.getBaseUrl()}/scanSerialNumbers`;
    return this.http.post(url, data);
  }

  getScanPercent(orderId: String){
    const data = { 'orderId': orderId };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getScanPercent`;
    return this.http.post(url, data);
  }

  getScanStatus(orderId: String){
    const data = { 'orderId': orderId };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getScanStatus`;
    return this.http.post(url, data);
  }

  generateUpsShippingImg(orderId: any){
    const data = { orderId: orderId };
    return this.http.post("http://localhost:8000/generateUpsShippingImg", data);
  }
}
