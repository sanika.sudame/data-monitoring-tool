import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class PackageService {

  constructor(private http: HttpClient, private commonApiServiceObj: commonApiService) { }

  // api call to fetch data from laravel // controller/fetchExamples..
  getPackages(orderId: any) {
    const data = { 'orderId': orderId };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getPackages`;
    return this.http.post(url, data);
  }

  // get package types // pallet // carton
  getPackageTypes() {
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getPackageTypes`;
    return this.http.get(url);
  }

  deletePackage(packageUid: any) {
    const data = { 'packageUid': packageUid };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/deletePackage`;
    return this.http.post(url, data)
  }

  // add new package associated with order id
  addNewPackage(orderId: any, packageType: any, upc: any, newPackageName: any, newPackageWeight: any) {
    const data = {
      'orderId': orderId,
      'packageType': packageType,
      'upc': upc,
      'newPackageName': newPackageName,
      'newPackageWeight': newPackageWeight
    }
    const url = `${this.commonApiServiceObj.getBaseUrl()}/addNewPackage`;
    return this.http.post(url, data);
  }

  // update name of the existing package
  editExistingPackage(packageUid: any, newPackageName: any) {
    const data = {
      'packageUid': packageUid,
      'newPackageName': newPackageName
    }
    const url = `${this.commonApiServiceObj.getBaseUrl()}/editExistingPackage`;
    return this.http.post(url, data);
  }

  // add tracking number to selected package name
  addTrackingNumber(orderId: any, packageUid: any, trackingNumber: any) {
    const data = {
      'orderId': orderId,
      'packageUid': packageUid,
      'trackingNumber': trackingNumber
    };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/addTrackingNumber`;
    return this.http.post(url, data);
  }

  // get all pick serial numbers
  getPickSerialNumbers(orderId: any) {
    const data = { 'orderId': orderId };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getPickSerialNumbers`;
    return this.http.post(url, data);
  }

  addPackagetoPickedSerialNumbers(orderId: any, userId: any, packageUid: any, serialNumbersArr: any) {
    const data = { 'orderId': orderId, 'userId': userId, 'packageUid': packageUid, 'serialNumbersArr': serialNumbersArr }
    const url = `${this.commonApiServiceObj.getBaseUrl()}/addPackagetoPickedSerialNumbers`;
    return this.http.post(url, data);
  }
}
