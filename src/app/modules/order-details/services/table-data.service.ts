import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { commonApiService } from 'src/app/services/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class tableDataService {

  constructor(public http: HttpClient, private commonApiServiceObj: commonApiService) { }

  getTableData(orderId: any, upc: any[]) {
    let params = new HttpParams();
    params = params.append('orderId', orderId);
    params = params.append('upc', upc.join(', '));
    const url = `${this.commonApiServiceObj.getBaseUrl()}/tableData`;
    return this.http.get(url, { params: params });
  }

  updateDeviceId(packageUid: any, serialNumber: any, deviceId: any) {
    const data = { 'packageUid': packageUid, 'serialNumber': serialNumber, 'deviceId': deviceId };
    const url = `${this.commonApiServiceObj.getBaseUrl()}/updateDeviceId`;
    return this.http.post(url, data);
  }
}
