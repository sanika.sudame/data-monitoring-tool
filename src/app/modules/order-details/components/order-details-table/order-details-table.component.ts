import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, RouteConfigLoadEnd, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { tableDataService } from '../../services/table-data.service';
import { SharedOrderDetailsService } from '../../../../shared/shared-order-details.service';
@Component({
  selector: 'app-order-details-table',
  templateUrl: './order-details-table.component.html',
  styleUrls: ['./order-details-table.component.scss']
})
export class OrderDetailsTableComponent implements OnChanges{
  @Input() orderId: any;
  @Input() upc: any;
  tableData: any;
  selectedValue: any;

  constructor(public route: ActivatedRoute,
    private tableDataService: tableDataService,
    private router: Router) {
  }

  ngOnInit() {
    this.getTableData(this.orderId, this.upc);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if('upc' in changes){
      console.log(this.upc);
      this.getTableData(this.orderId, this.upc);
    }
  }

  getTableData(orderId: any, upc: any[]) {
    this.tableDataService.getTableData(orderId, upc)
      .subscribe((data) => {
        console.log(data);
        this.tableData = data;
      },
        (error) => {
          console.error('Error occurred:', error);
        });
  }

  navToFindOldInventory(orderId: any, upc: any) {
    console.log(orderId + ' ' + upc)
    this.router.navigate(["/find-oldest-inv"]);
  }

  submitUpdateDeviceIdForm(packageUid: any, serialNumber: any, deviceId: any) {
    this.tableDataService.updateDeviceId(packageUid, serialNumber, deviceId)
      .subscribe((data) => {
        alert("Device Id updated");
      }, (error) => {
        alert("Failed" + error);
      })
      this.getTableData(this.orderId, this.upc);
  }

  findOldestInventory(upcCode: any){
    this.router.navigate(['find-oldest-inv', this.orderId, upcCode])
  }
}