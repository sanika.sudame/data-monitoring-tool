import { Component, OnInit, ViewChild, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import 'bootstrap';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import { PackageService } from '../../services/package.service';
import { OrderDetailsService } from '../../services/order-details.service';
import { OrderDetailsTableComponent } from '../order-details-table/order-details-table.component';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { SharedService } from 'src/app/shared/shared.service';
import { UpsAuthService } from 'src/app/auth/services/ups-auth.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
})

export class OrderDetailsComponent {
  orderId: any;
  userId: string = "";
  userName: string = "";
  scannedSerialNumbers: any;
  selectedPackage: string = "";
  selectedUpc: number = 0;
  packageTypes: any;
  res: any;
  tableData: any;
  formModal: any;
  packageName: any;
  trackingNumber: string = "";
  upc: any;
  allPackages: any;
  upcCode: any; // array of all upc values associated with particular order id
  pickSerialNumbers: any;
  checkedSerialNumbers: String[] = [];
  isSelectPackageLabelVisible = false;

  // progress bar..
  color: ThemePalette = 'warn';
  mode: ProgressSpinnerMode = 'determinate';
  progressValue = 100;

  @ViewChild('orderDetailsTableComponent', { static: false }) orderDetailsTableComponent!: OrderDetailsTableComponent;

  constructor(public packageDataServiceObj: PackageService,
    private router: Router,
    public orderDetailsServiceObj: OrderDetailsService,
    private route: ActivatedRoute,
    private upsAuthServiceObj: UpsAuthService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.orderId = params.get('orderId');
      this.getPackages(this.orderId);
      this.getPickSerialNumbers(this.orderId);
      this.getUpcFunction(this.orderId);
      this.getPackageTypes();
      this.getScanPercent(this.orderId);
      this.getScanStatus(this.orderId); // for green click on scan and pack status 
      this.getOrderDetailsTableData();
    });

    this.userId = sessionStorage.getItem('userId') as string;
    this.userName = sessionStorage.getItem('userName') as string;
  }

  getPackages(orderId: any) {
    this.packageDataServiceObj.getPackages(this.orderId)
      .subscribe((data: any) => {
        console.log(data)
        this.allPackages = data.payload;
      }, (error: any) => {
        console.log(error)
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        console.log("get all packages failed!!" + error.error);
      });
  }

  // get all upc values to order id
  getUpcFunction(orderId: any) {
    this.orderDetailsServiceObj.getUpcValues(orderId)
      .subscribe((data: any) => {
        this.upcCode = data.payload;
      }, (error) => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        console.log("Failed to get upc values" + error)
      });
  }

  getPackageTypes() {
    this.packageDataServiceObj.getPackageTypes()
      .subscribe((data: any) => {
        this.packageTypes = data.payload;
      }, (error: any) => {
        console.log(error)
        if (error.status == 401) {
          this.router.navigate(['/login']);
        }
        console.log("Failed to get package types" + error.error);
      })
  }

  // scan form validator
  scanForm = new FormGroup({
    package: new FormControl(''),
    formUpc: new FormControl('', [Validators.required]),
    serial_number: new FormControl('', [Validators.required])
  })

  // red label for scan form fields
  get package() {
    return this.scanForm.get('package');
  }
  get formUpc() {
    return this.scanForm.get('formUpc');
  }
  get serial_number() {
    return this.scanForm.get('serial_number')
  }

  // form to add new package
  addNewPackageForm = new FormGroup({
    packageType: new FormControl('', [Validators.required]),
    upc: new FormControl('', [Validators.required]),
    newPackageName: new FormControl('', [Validators.required]),
    newPackageWeight: new FormControl('', [Validators.required])
  });

  // red label for add new package modal form fields
  get packageType() {
    return this.addNewPackageForm.get('packageType');
  }
  get upcForm() {
    return this.addNewPackageForm.get('upc');
  }
  get newPackageName() {
    return this.addNewPackageForm.get('newPackageName')
  }
  get newPackageWeight() {
    return this.addNewPackageForm.get('newPackageWeight')
  }

  // form to add new tracking number modal
  addTrackingNumberForm = new FormGroup({
    package: new FormControl('', [Validators.required]),
    newTrackingNumber: new FormControl('', [Validators.required])
  })

  get newTrackingNumber() {
    return this.addTrackingNumberForm.get('newTrackingNumber');
  }

  // form to edit package name modal
  editPackageNameForm = new FormGroup({
    updatedPackageName: new FormControl('', [Validators.required]),
    package: new FormControl('', [Validators.required])
  });

  // red label to edit package modal form field
  get updatedPackageName() {
    return this.editPackageNameForm.get('updatedPackageName');
  }

  editPackageName() {
    const packageUid = this.editPackageNameForm.value.package;
    const newPackageName = this.editPackageNameForm.value.updatedPackageName;
    this.packageDataServiceObj.editExistingPackage(packageUid, newPackageName)
      .subscribe((data: any) => {
        alert("Package name updated successfully");
        this.getPackages(this.orderId); // get all the updated packages 
        this.closeEditPackageModal(); // close the modal
        this.getOrderDetailsTableData(); //get updated table data 
      }, (error) => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        alert(error);
      })
    // reset the form to null
    this.editPackageNameForm.reset({
      package: '',
      updatedPackageName: ''
    });
  }

  // toggle modals for edit package
  showeEditPackageModal() {
    const modalDiv = document.getElementById('editPackageModal');
    if (modalDiv != null) {
      modalDiv.style.display = 'block';
    }
  }
  closeEditPackageModal() {
    const modalDiv = document.getElementById('editPackageModal');
    if (modalDiv != null) {
      modalDiv.style.display = 'none';
    }
  }

  // toggle modals - make serial numbers pick to pack // allocate package for scanned serial numbers
  addPackagetoSerialNumberModal() {
    const modalDiv = document.getElementById("check_serial_no");
    if (modalDiv != null) {
      modalDiv.style.display = 'block';
    }
  }
  closeAddPackagetoSerialNumberModal() {
    const modalDiv = document.getElementById('check_serial_no');
    if (modalDiv != null) {
      modalDiv.style.display = 'none';
    }
  }

  // toggle modals for create new package
  createPackage() {
    const modalDiv = document.getElementById("createNewPackage");
    if (modalDiv != null) {
      modalDiv.style.display = 'block';
    }
  }
  closePackageModal() {
    const modalDiv = document.getElementById('createNewPackage');
    if (modalDiv != null) {
      modalDiv.style.display = 'none';
    }
  }

  // toggle modals for edit package
  showAddTrackingNumberModal() {
    const modalDiv = document.getElementById("addTrackingNumberModal");
    if (modalDiv != null) {
      modalDiv.style.display = 'block';
    }
  }
  closeAddTrackingNumberModal() {
    const modalDiv = document.getElementById('addTrackingNumberModal');
    if (modalDiv != null) {
      modalDiv.style.display = 'none';
    }
  }

  // scan serial number with or without package
  scannedSerialNumber() {
    let serial_numbers = this.scanForm.value.serial_number;
    let serial_numbers_arr = serial_numbers?.split('\n')
    let unique_serial_numbers = new Set(serial_numbers_arr);
    serial_numbers_arr = Array.from(unique_serial_numbers);

    const upcValue = this.scanForm.value.formUpc;
    this.orderDetailsServiceObj.scanSerialNumber(this.orderId, upcValue, serial_numbers_arr, 21, this.scanForm.value.package)
      .subscribe((data: any) => {
        alert("Serial numbers successfully scanned");
        this.getOrderDetailsTableData();
        this.getPickSerialNumbers(this.orderId);
        this.getScanPercent(this.orderId);
        this.getScanStatus(this.orderId);
      }, (error) => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        alert(error);
      });
    // reset scan form values
    this.scanForm.reset({
      package: '',
      formUpc: '',
      serial_number: ''
    });
  }
  // Function to be called on the button click
  public getOrderDetailsTableData(): void {
    // Call the function in the child component
    this.orderDetailsTableComponent.getTableData(this.orderId, this.upcCode);
  }
  addNewPackage() {
    const packageType = this.addNewPackageForm.value.packageType
    const upc = this.addNewPackageForm.value.upc
    const newPackageName = this.addNewPackageForm.value.newPackageName
    const newPackageWeight = this.addNewPackageForm.value.newPackageWeight

    this.packageDataServiceObj.addNewPackage(this.orderId, packageType, upc, newPackageName, newPackageWeight)
      .subscribe((data: any) => {
        alert("Package added!");
        this.getPackages(this.orderId);
      }, (error) => {
        alert("Package not added");
        console.log(error);
      });
    this.addNewPackageForm.reset({
      packageType: '',
      upc: '',
      newPackageName: ''
    });
   
    this.closePackageModal();
  }
  addTrackingNumber() {
    const trackingId = this.addTrackingNumberForm.value.newTrackingNumber;
    const packageUid = this.addTrackingNumberForm.value.package;
    this.packageDataServiceObj.addTrackingNumber(this.orderId, packageUid, trackingId).subscribe((data) => {
      alert('Tracking Id Updated');
      this.closeAddTrackingNumberModal();
      this.getScanStatus(this.orderId);
    },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        alert('Update request failed' + error)
      });
  }

  // delete selected package
  deletePackage() {
    this.packageDataServiceObj.deletePackage(this.selectedPackage)
      .subscribe((data: any) => {
        console.log(data);
        alert("Package deleted successfully!");
        this.getOrderDetailsTableData();
        this.getPackages(this.orderId);
        this.getScanPercent(this.orderId);
      }, (error: any) => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        alert("Failed to delete package");
        console.log(error.error);
        this.getOrderDetailsTableData();
        this.getScanPercent(this.orderId);
      })
    // reset scan form values
    this.scanForm.reset({
      package: '',
      formUpc: '',
      serial_number: ''
    });
  }
  onPackageChange(event: any) {
    this.selectedPackage = event.target.value;
  }

  getPickSerialNumbers(orderId: any) {
    this.packageDataServiceObj.getPickSerialNumbers(orderId).subscribe((data: any) => {
      this.pickSerialNumbers = data.payload;
      this.getScanStatus(this.orderId);
    }, (error) => {
      if (error.status == 401) {
        this.router.navigate(['/login']);
        alert('token expired')
      }
      console.log(error);
    })
  }

  onCheckboxChange(event: any, serialNumber: string, macAddress: string) {
    if (event.checked) {
      // Checkbox unchecked
      this.checkedSerialNumbers.push(serialNumber);
      // event.serialNumber.isChecked = false;
    }
    else {
      // Checkbox is unchecked
      const index = this.checkedSerialNumbers.indexOf(serialNumber)
      if (index !== -1) {
        this.checkedSerialNumbers.splice(index, 1);
      }
    }
  }

  // add packages to pick serial numbers
  addPackagetoPickSerialNumbers() {
    this.isSelectPackageLabelVisible = false;
    const packageUid = this.scanForm.value.package;
    console.log(this.checkedSerialNumbers);
    if (packageUid) {
      this.packageDataServiceObj.addPackagetoPickedSerialNumbers(this.orderId, 21, packageUid, this.checkedSerialNumbers)
        .subscribe((data: any) => {
          console.log(data);
          this.closeAddPackagetoSerialNumberModal();
          this.getPickSerialNumbers(this.orderId);
          this.getOrderDetailsTableData();
          this.getScanPercent(this.orderId);
        }, (error) => {
          if (error.status == 401) {
            this.router.navigate(['/login']);
            alert('token expired')
          }
          console.log(error);
        });

    }
    else {
      this.isSelectPackageLabelVisible = true;
    }
  }

  getScanPercent(orderId: String) {
    this.orderDetailsServiceObj.getScanPercent(orderId)
      .subscribe((data: any) => {
        this.progressValue = parseInt(data.payload);
      }, (error) => {
        this.progressValue = 0;
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        console.log(error)
      })
  }

  getScanStatus(orderId: String) {
    this.orderDetailsServiceObj.getScanStatus(orderId)
      .subscribe((data: any) => {
        console.log(data);
        if (data.scanStatus == true) {
          var imageElement = document.getElementById("scan-stepper") as HTMLImageElement | null;
          if (imageElement) {
            imageElement.src = "../assets/check-circle.svg";
          }
          if (data.packStatus == true) {
            var imageElement2 = document.getElementById("pack-stepper") as HTMLImageElement | null;
            if (imageElement2) {
              imageElement2.src = "../assets/check-circle.svg";
            }
            if (data.trackStatus == true) {
              var imageElement3 = document.getElementById("track-stepper") as HTMLImageElement | null;
              if (imageElement3) {
                imageElement3.src = "../assets/check-circle.svg";
              }
            }
          }
        }
      }, (error) => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
          alert('token expired')
        }
        console.log(error);
      })
  }

  shipAndPrint() {
    this.orderDetailsServiceObj.generateUpsShippingImg(this.orderId).subscribe((data: any) => {
      if (data.ShipmentResponse) {
        let graphicLabel = data.ShipmentResponse.ShipmentResults.PackageResults.ShippingLabel.GraphicImage;
        this.createImage(graphicLabel);
      }
    }, (error: any) => {
      console.log(error);
    });
  }

  // shipping image from ups server
  createImage(graphicLabel: any) {
    const img = new Image();
    img.src = 'data:image/png;base64,' + graphicLabel;
    img.height = 400;
    img.style.zIndex = '20';
    img.classList.add('rotateimg90');
    img.style.transform = 'rotate(90deg)';
    img.style.marginTop = '165px';

    const newWindow = window.open('', 'Image Window', 'width=750,height=750');
    if (newWindow) {
      newWindow.document.body.appendChild(img);
    } else {
      alert('Popup blocker may be preventing the new window. Please check your browser settings.');
    }
  }
}
