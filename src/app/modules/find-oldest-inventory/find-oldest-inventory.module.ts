import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindOldestInventoryComponent } from './components/find-oldest-inventory/find-oldest-inventory.component';
import { ScanSerialNumberComponent } from './components/scan-serial-number/scan-serial-number.component';
import { OldInvTableComponent } from './components/old-inv-table/old-inv-table.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [
    FindOldestInventoryComponent,
    ScanSerialNumberComponent,
    OldInvTableComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule
  ]
})
export class FindOldestInventoryModule { }
