import { TestBed } from '@angular/core/testing';

import { FindOldestInventoryService } from './find-oldest-inventory.service';

describe('FindOldestInventoryService', () => {
  let service: FindOldestInventoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FindOldestInventoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
