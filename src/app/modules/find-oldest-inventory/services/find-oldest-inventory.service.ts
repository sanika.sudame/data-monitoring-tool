import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';
import { OrderDetailsComponent } from '../../order-details/components/order-details/order-details.component';

@Injectable({
  providedIn: 'root'
})
export class FindOldestInventoryService {

  constructor(public http: HttpClient, private commonApiServiceObj: commonApiService) { }

  getOldInvData(orderId: any, upcCode: any){
    const data = { orderId: orderId, upcCode: upcCode}
    const url = `${this.commonApiServiceObj.getBaseUrl()}/findOldInventory`;
    return this.http.post(url, data);
  }

  removeSerialNumber(orderId: any, serialNumber: any){
    const data = { orderId: orderId, serialNumber: serialNumber}
    const url = `${this.commonApiServiceObj.getBaseUrl()}/removeSerialNumber`;
    return this.http.post(url, data);
  }

  reviewUpc(orderId: any, upcCode: any, userId: any){
    const data = { orderId: orderId, upcCode: upcCode, userId: userId};
    const url = `${this.commonApiServiceObj.getBaseUrl()}/reviewUpc`;
    return this.http.post(url, data);
  }

  getMappedUpc(orderId: any, upcCode: any){
    const data = { orderId: orderId, upcCode: upcCode};
    const url = `${this.commonApiServiceObj.getBaseUrl()}/getMappedUpc`;
    return this.http.post(url, data);
  }
}
