import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OrderDetailsService } from 'src/app/modules/order-details/services/order-details.service';
import { SharedOrderDetailsService } from '../../../../shared/shared-order-details.service';
import { tableDataService } from 'src/app/modules/order-details/services/table-data.service';
import { sharedOldInvService } from '../../../../shared/sharedOldInv.service';
import { OldInvTableComponent } from '../old-inv-table/old-inv-table.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-scan-serial-number',
  templateUrl: './scan-serial-number.component.html',
  styleUrls: ['./scan-serial-number.component.scss']
})
export class ScanSerialNumberComponent {
  selectedValue: { orderId: any, upcCode: any } | null = null;
  orderId: any;
  upcCode: any;
  mappedUpcCode: number = 0;
  serialNumberArr: any = [];
  checkedSerialNumberArr: any = [];

  constructor(private sharedOrderDetailsServiceObj: SharedOrderDetailsService,
    public route: ActivatedRoute,
    private scanServiceObj: OrderDetailsService,
    private sharedOldInvServiceObj: sharedOldInvService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.orderId = params.get('orderId');
      this.upcCode = params.get('upc');
    })

    this.sharedOldInvServiceObj.serialNumbers$.subscribe((serialNumbers) => {
      this.checkedSerialNumberArr = serialNumbers;
    });

    this.sharedOldInvServiceObj.mappedUpcCode.subscribe((upcCode: number) => {
      this.mappedUpcCode = upcCode;
      console.log(this.mappedUpcCode);
      if(this.mappedUpcCode == 0){
        console.log('00')
      }
      else{
        this.upcCode = upcCode;
      }
    })
  }
  // scan form validator
  scanForm = new FormGroup({
    serial_number: new FormControl('')
  })

  scannedSerialNumbers() {
    this.serialNumberArr[0] = this.scanForm.value.serial_number;
    this.serialNumberArr = this.serialNumberArr.concat(this.checkedSerialNumberArr);
    console.log(this.serialNumberArr);
    console.log(this.upcCode)
    // if(this.mappedUpcCode != 0){
    //   // this.upcCode = this.mappedUpcCode
    // }
    this.scanServiceObj.scanSerialNumber(this.orderId, this.upcCode, this.serialNumberArr, sessionStorage.getItem("userId"), null)
      .subscribe((data) => {
        console.log(data);
        alert('Serial number added to pick');
        this.sharedOldInvServiceObj.refreshOldInventoryTable(true);
      }, (error) => {
        console.log(error);
      });
    this.scanForm.reset({
      serial_number: ''
    })
  }

  sendSerialNumbers(serialNumbers: string[]): void {
    this.sharedOldInvServiceObj.setSerialNumbers(serialNumbers);
  }
}
