import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FindOldestInventoryService } from '../../services/find-oldest-inventory.service';

@Component({
  selector: 'app-find-oldest-inventory',
  templateUrl: './find-oldest-inventory.component.html',
  styleUrls: ['./find-oldest-inventory.component.scss']
})
export class FindOldestInventoryComponent {
  selectedValue: { orderId: any, upcCode: any } | null = null;
  serialNumberArr: any=[];
  orderId: any;
  upcCode : any;

  constructor( private findOldInvServiceObj: FindOldestInventoryService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
      this.route.paramMap.subscribe((params)=>{
        this.orderId = params.get('orderId');
        this.upcCode = params.get('upc')
      })
  }
}
