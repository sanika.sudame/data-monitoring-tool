import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindOldestInventoryComponent } from './find-oldest-inventory.component';

describe('FindOldestInventoryComponent', () => {
  let component: FindOldestInventoryComponent;
  let fixture: ComponentFixture<FindOldestInventoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FindOldestInventoryComponent]
    });
    fixture = TestBed.createComponent(FindOldestInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
