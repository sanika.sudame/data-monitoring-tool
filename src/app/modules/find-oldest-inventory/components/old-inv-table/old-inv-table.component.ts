import { Component, ElementRef, Renderer2 } from '@angular/core';
import { SharedOrderDetailsService } from '../../../../shared/shared-order-details.service';
import { FindOldestInventoryService } from '../../services/find-oldest-inventory.service';
import { sharedOldInvService } from '../../../../shared/sharedOldInv.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-old-inv-table',
  templateUrl: './old-inv-table.component.html',
  styleUrls: ['./old-inv-table.component.scss']
})
export class OldInvTableComponent {
  orderId: string= "";
  upcCode: any;
  tableData: any;
  checkedSerialNumber: any = [];
  selectedValue: { orderId: any; upcCode: any; } | null = null;
  oldUpc: any;
  mappedUpc: number = 0;

  constructor(private oldInvServiceObj: FindOldestInventoryService,
    public sharedOrderDetailsServiceObj: SharedOrderDetailsService,
    public route: ActivatedRoute,
    private renderer: Renderer2,
    private notifyNote: ElementRef,
    private mappedNote: ElementRef,
    private sharedOldInvServiceObj: sharedOldInvService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.orderId = params.get('orderId') as string;
      this.upcCode = params.get('upc');
      this.getOldInvData(this.orderId, this.upcCode);
    })
    this.sharedOldInvServiceObj.refreshFlag$.subscribe((data) => {
      this.getOldInvData(this.orderId, this.upcCode);
    });
  }

  getOldInvData(orderId: any, upcCode: any) {
    this.oldInvServiceObj.getOldInvData(orderId, upcCode).subscribe((data: any) => {
      this.tableData = data.payload;
      // this.tableData.paginator = this.paginator;
      if (this.tableData.length == 0) {
        this.oldUpc = this.upcCode;
        this.renderer.setStyle(document.getElementById("map-upc-code"), 'display', 'block');
        this.renderer.setStyle(document.getElementById("table"), 'display', 'none');
        // check for mapped upc
        this.oldInvServiceObj.getMappedUpc(this.orderId, this.upcCode).subscribe((data: any) => {
          console.log(data);
          if(data.payload){
            this.upcCode = data.payload;
            this.mappedUpc = data.payload;
            this.sharedOldInvServiceObj.setUpcCode(this.mappedUpc);
            this.getOldInvData(this.orderId, this.upcCode);
            this.renderer.setStyle(document.getElementById("table"), 'display', 'flex');
            this.renderer.setStyle(document.getElementById("mappedNote"), 'display', 'block');
          }
        }, (error: any) => {
          console.log(error)
        })
      }
    }, (error) => {
      console.log(error)
    })
  }

  checkboxChanged(event: any, serialNumber: string): void {
    if (event.checked) {
      this.checkedSerialNumber.push(serialNumber)
    } else {
      const index = this.checkedSerialNumber.indexOf(serialNumber);
      if (index !== -1) {
        this.checkedSerialNumber.splice(index, 1);
      }
      console.log(`Checkbox unchecked for serial number: ${serialNumber}`);
    }
  }

  sendCheckedSerialNumber() {
    console.log(this.checkedSerialNumber);
  }

  sendSerialNumbers(): void {
    this.sharedOldInvServiceObj.setSerialNumbers(this.checkedSerialNumber);
  }

  removeSerialNumber(serialNumber: any) {
    console.log(serialNumber);
    this.oldInvServiceObj.removeSerialNumber(this.orderId, serialNumber).subscribe((data) => {
      console.log(data);
      this.getOldInvData(this.orderId, this.upcCode);
    }, (error) => {
      console.log(error);
    })
  }

  reviewUpc() {
    this.oldInvServiceObj.reviewUpc(this.orderId, this.upcCode, sessionStorage.getItem('userId'))
      .subscribe((data: any) => {
        console.log(data);
        if (data.payload) {
          console.log('already exists' + data.payload)
          this.mappedUpc = data.payload;
          this.upcCode = this.mappedUpc;
          this.getOldInvData(this.orderId, this.upcCode)
        }
        else {
          this.renderer.setStyle(document.getElementById("notifyNote"), 'display', 'block');
          console.log('notified')
        }
      }, (error) => {
        console.log(error);
      });
  }
}
