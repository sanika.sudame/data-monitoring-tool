import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OldInvTableComponent } from './old-inv-table.component';

describe('OldInvTableComponent', () => {
  let component: OldInvTableComponent;
  let fixture: ComponentFixture<OldInvTableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OldInvTableComponent]
    });
    fixture = TestBed.createComponent(OldInvTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
