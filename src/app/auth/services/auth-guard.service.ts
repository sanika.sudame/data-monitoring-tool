import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { map, Observable } from "rxjs";
import { commonApiService } from "src/app/services/common-api.service";

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private http: HttpClient, private commonApiServiceObj: commonApiService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.adminGuard().pipe(
            map((data: any) => {
                if (data.success) {
                    return true;
                }
                else {
                    console.log('failed')
                    return false;
                }
            })
        )
    }

    adminGuard(): Observable<any> {
        const data = { userId: sessionStorage.getItem("userId") };
        const url = `${this.commonApiServiceObj.getBaseUrl()}/adminGuard`;
        return this.http.post(url, data);
    }
}