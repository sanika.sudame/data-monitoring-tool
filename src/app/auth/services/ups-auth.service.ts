import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UpsAuthService {

  private apiUrl = 'https://wwwcie.ups.com/security/v1/oauth/token';
  private clientId = '6XVJ2OalZ6MmbD6FUQmR7cL24SdG3VXzRl7tovL0pstJhs1e';
  private clientSecret = 'bY3X6doFCSAaBDSx2H316FHpU5tMNKRpPLJLT0LXG53piOFOV5QEM2f8kKbGy5c8';
  private merchantId: string = '6XVJ2OalZ6MmbD6FUQmR7cL24SdG3VXzRl7tovL0pstJhs1e';
  private httpClient: HttpClient;

  constructor(private handler: HttpBackend, private http: HttpClient) {
    this.httpClient = new HttpClient(handler);
  }

  generateUpsToken2() {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-merchant-id': '6XVJ2OalZ6MmbD6FUQmR7cL24SdG3VXzRl7tovL0pstJhs1e',
      'Authorization': 'Bearer' + btoa("6XVJ2OalZ6MmbD6FUQmR7cL24SdG3VXzRl7tovL0pstJhs1e:bY3X6doFCSAaBDSx2H316FHpU5tMNKRpPLJLT0LXG53piOFOV5QEM2f8kKbGy5c8")
    });

    const data = { 'grant_type': "client_credentials" }
    return this.http.post("https://wwwcie.ups.com/security/v1/oauth/token", data)
  }

  generateUpsToken() {
    const payload: string = 'grant_type=client_credentials';
    const auth = "Basic NlhWSjJPYWxaNk1tYkQ2RlVRbVI3Y0wyNFNkRzNWWHpSbDd0b3ZMMHBzdEpoczFlOmJZM1g2ZG9GQ1NBYUJEU3gySDMxNkZIcFU1dE1OS1JwUExKTFQwTFhHNTNwaU9GT1Y1UUVNMmY4a0tiR3k1Yzg=";

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-merchant-id': this.merchantId,
      'Authorization': auth,
      // 'Access-Control-Allow-Origin': '*',
    });

    console.log(headers);
    this.httpClient.post(this.apiUrl, 'grant_type=client_credentials', { headers }).subscribe((res)=>{
      console.log(res);
    }, (error)=>{
      console.log(error);
    });
  }

  generateUpsToken3(){
    // this.http.get("http://localhost:8000/generateUpsAuthToken").subscribe((data: any)=>{
    //   return data.data;
    // }, (error: any)=>{
    //   return error;
    // })
    return this.http.get("http://localhost:8000/generateUpsAuthToken", {"responseType": "text"});
  }
}
