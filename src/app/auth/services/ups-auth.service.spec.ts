import { TestBed } from '@angular/core/testing';

import { UpsAuthService } from './ups-auth.service';

describe('UpsAuthService', () => {
  let service: UpsAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpsAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
