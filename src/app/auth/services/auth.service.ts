import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { commonApiService } from 'src/app/services/common-api.service';
import { SharedService } from 'src/app/shared/shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, 
    private commonApiServiceObj: commonApiService, 
    private isLoggedInSubject: SharedService) { }

  setAuthToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getAuthToken(): string | null {
    return localStorage.getItem('token');
  }

  logout() {
    const token = this.getAuthToken();
    this.isLoggedInSubject.logout();
    const data = { token: token }
    const url = `${this.commonApiServiceObj.getBaseUrl()}/logout`;
    return this.http.post(url, data);
  }
}
