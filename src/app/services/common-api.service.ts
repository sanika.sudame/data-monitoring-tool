import { Injectable } from "@angular/core";

Injectable({
    providedIn: 'root'
  })
  export class commonApiService{
    private baseUrl: string = 'http://127.0.0.1:8000';

    getBaseUrl(): string{
        return this.baseUrl;
    }
}