import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { OrdersToFulfillComponent } from './modules/orders-to-fulfill/components/orders-to-fulfill/orders-to-fulfill.component';
import { OrderDetailsComponent } from './modules/order-details/components/order-details/order-details.component';
import { SummaryComponent } from './modules/summary/components/summary/summary.component';
import { LoginComponent } from './modules/login/component/login/login.component';
import { FindOldestInventoryComponent } from './modules/find-oldest-inventory/components/find-oldest-inventory/find-oldest-inventory.component';
import { SignupComponent } from './modules/signup/components/signup/signup.component';
import { OrdersReviewComponent } from './modules/orders-review/components/orders-review/orders-review.component';
import { AuthGuardService } from './auth/services/auth-guard.service';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'summary', component: SummaryComponent },
  { path: 'orders-to-fulfill', component: OrdersToFulfillComponent },
  { path: 'order-details/:orderId', component: OrderDetailsComponent },
  { path: 'find-oldest-inv/:orderId/:upc', component: FindOldestInventoryComponent },
  { path: 'orders-review', component: OrdersReviewComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), MatInputModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
