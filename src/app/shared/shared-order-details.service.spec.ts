import { TestBed } from '@angular/core/testing';

import { SharedOrderDetailsService } from './shared-order-details.service';

describe('SharedOrderDetailsService', () => {
  let service: SharedOrderDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SharedOrderDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
