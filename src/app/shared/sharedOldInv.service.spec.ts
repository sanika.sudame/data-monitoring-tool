import { TestBed } from '@angular/core/testing';

import { sharedOldInvService } from './sharedOldInv.service';

describe('SharedServiceService', () => {
  let service: sharedOldInvService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(sharedOldInvService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
