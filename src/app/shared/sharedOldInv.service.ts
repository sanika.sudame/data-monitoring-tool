import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class sharedOldInvService {

  constructor() { }

  private serialNumberSubject = new BehaviorSubject<string[]>([]);
  public serialNumbers$ = this.serialNumberSubject.asObservable();

  private refreshFlagSubject = new BehaviorSubject<boolean>(false);
  public refreshFlag$ = this.refreshFlagSubject.asObservable();

  private mappedUpcCodeSubject = new BehaviorSubject<number>(0);
  public mappedUpcCode = this.mappedUpcCodeSubject.asObservable();

  setSerialNumbers(serialNumbers: string[]): void{
    this.serialNumberSubject.next(serialNumbers);
  }

  refreshOldInventoryTable(flag: boolean): void{
    this.refreshFlagSubject.next(true);
  }

  setUpcCode(upcCode: number): void{
    this.mappedUpcCodeSubject.next(upcCode);
  }
}
