import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})

export class SharedService {

    private isLoggedInSubject = new BehaviorSubject<boolean>(false);
    isLoggedIn$ = this.isLoggedInSubject.asObservable();

    constructor() { }

    login() {
        sessionStorage.setItem('login', 'true');
        this.isLoggedInSubject.next(true);
    }

    logout() {
        sessionStorage.setItem('login', 'false');
        this.isLoggedInSubject.next(false);
    }

}