import { Component } from '@angular/core';
import { SharedService } from './shared/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'data-monitoring-tool';
  isLoggedIn = false;

  constructor(private isLoggesInService: SharedService){}

  ngOnInit(){
    this.isLoggesInService.isLoggedIn$.subscribe((loggedIn: boolean)=>{
      let strLogin = sessionStorage.getItem('login')
      if(strLogin == 'true'){
        this.isLoggedIn = true;
        console.log('isloggedin'+ true)
      }
      else{
        this.isLoggedIn = false;
        console.log('isloggedin'+ false)
      }
    })
  }
}
