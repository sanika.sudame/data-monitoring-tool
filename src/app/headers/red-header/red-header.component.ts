import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-red-header',
  templateUrl: './red-header.component.html',
  styleUrls: ['./red-header.component.scss']
})
export class RedHeaderComponent {
  opened: boolean = false;
  userName: any;
  showFiller = false;

  constructor(private router: Router,
    public authServiceObj: AuthService) { }

  ngOnInit() {
    this.userName = sessionStorage.getItem('userName');
    console.log(this.userName);
  }
  toggleSidenav() {
    this.opened = !this.opened;
  }



  logout() {
    const userConfirm = window.confirm("Are you sure you want to log out?")
    if (userConfirm) {
      this.authServiceObj.logout().subscribe((data: any) => {
        if (data.success) {
          this.router.navigate(['/login']);
        }
      }, (error: any) => {
        alert("Failed to logout");
        console.log(Object.entries(error));
      });
    }
  }
}
