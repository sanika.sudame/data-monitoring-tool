import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.scss']
})
export class Header2Component {
  opened: boolean = false;
  userName: any;

  constructor(
    private router: Router,
    public authServiceObj: AuthService) { }

  ngOnInit() {
    this.userName = sessionStorage.getItem('userName')
  }


  toggleSidenav() {
    this.opened = !this.opened;
  }

  getAuthToken(){
    return sessionStorage.getItem('Name');
  }

  logout() {
    const userConfirm = window.confirm("Are you sure you want to log out?")
    if (userConfirm) {
      this.authServiceObj.logout().subscribe((data: any) => {
        if (data.success) {
          this.router.navigate(['/login']);
        }
      }, (error: any) => {
        alert("Failed to logout");
        console.log(Object.entries(error));
      });
    }
  }
}
